$(function(){
    var form = $("form");
    var form_lien = $("#form_lien", form);

    function verifyLink(){
        var lien = form_lien.val(); 
        var re = /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
        if(re.test(lien)){
            return true    
        }else{
            return false;        
        }
    }

    form_lien.blur(function(){
        
        if(verifyLink()){
            $(this).css("border", "solid 2px green");
        }else{
            $(this).css("border", "solid 2px red");
        }      

    })

    form.submit(function(event){
        if(!verifyLink()){
            event.preventDefault();
        }
    })
})
<?php

/**
* @author Abdelilah Benhima <abdelilahbenhima@gmail.com>
*/

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Resources\views;
use AppBundle\Entity\Videos;
use AppBundle\Entity\Tags;
use AppBundle\Entity\Commentaires;
use AppBundle\Validator\Constraints\YouTubeURL;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Knp\Component\Pager\Paginator;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $videosRepo = $this->getDoctrine()->getManager()->getRepository('AppBundle:Videos');
        $videos = $videosRepo->findBy(array(), array(), 8, 0);
        
        return array('videos' => $videos);
    }

    /**
     * @Route("/publier", name="upload")
     * @Template()
     */
    public function uploadAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $tags = $em->getRepository('AppBundle:Tags')->findAll();
        
        $video = new Videos();

        $form = $this->createFormBuilder($video)
            ->add('lien', TextType::Class, array(
                'label' => 'Lien',
                /*'constraints' => array( new YouTubeURL())*/)
                )
            ->add('pseudo')
            ->add('description')
            ->add('tag', ChoiceType::Class, array(
                'choices' => $tags,
                'choice_label' => 'Tag')
                )
            ->add('publier', SubmitType::Class)
            ->getForm();

          $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $video = $form->getData()
             ->setEtat(false);
            $info = $video->getYoutubeInfo();
            $video->setTitre($info['title']);
            $em->persist($video);
            $em->flush();

            $this->addFlash('upload_message', 'Votre lien à bien été soumis');
            
            return $this->redirectToRoute('upload');
        }

            return array('form' => $form->createView());
    }

     /**
      * @Route("/videos", name="videos")
      * @Template()
      */
    public function videosAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $dql = "SELECT video FROM AppBundle:Videos video";
        $query;

        //getting search params
        $rubrique = $request->query->getInt('rubrique', null);
        $searchString = $request->query->get('search_string', null);
        
        if($rubrique != null &&  $searchString != null && $rubrique != 0 && $searchString != '')
        {
            $dql = "SELECT video FROM AppBundle:Videos video WHERE video.tag = :tag AND video.titre LIKE :titre";
            $params = array(
                'tag' => $rubrique,
                'titre' => '%'.$searchString.'%'
            );
            $query = $em->createQuery($dql)
                        ->setParameter($params);

        }
        else if($searchString != null && $searchString != '')
        {
            $dql = "SELECT video FROM AppBundle:Videos video WHERE video.titre LIKE :titre";
            $query = $em->createQuery($dql)
                        ->setParameter('titre', '%'.$searchString.'%');
        }
        else if($rubrique != null && $rubrique != 0)
        {
            $dql = "SELECT video FROM AppBundle:Videos video WHERE video.tag = :tag";
            $query = $em->createQuery($dql)
                        ->setParameter('tag', $rubrique);
        }
        else
        {
            $query = $em->createQuery($dql);
        }


        $tags = $em->getRepository('AppBundle:Tags')->findAll();

        $paginator = $this->get('knp_paginator');
        $result = $paginator->paginate($query, $request->query->getInt('page', 1), 8);

        return array('tags' => $tags,
                    'videos' => $result);
    }

      /**
       * @Route("/video/{id}/{title}", name="watch")
       * @Template()
       */
    public function watchAction(Request $request, $id, $title)
    {
        $em = $this->getDoctrine()->getManager();
        $video = $em->getRepository('AppBundle:Videos')->findOneBy(array('id' => $id));
        $commentaires = $em->getRepository('AppBundle:Commentaires')->findBy(array('video' => $video));

        $commentaire = new Commentaires();

        $form = $this->createFormBuilder($commentaire)
            ->add('pseudo')
            ->add('commentaire')
            ->add('Envoyer', SubmitType::Class)
            ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $commentaire = $form->getData()
                ->setVideo($video)
                ->setDate();
            $em->persist($commentaire);
            $em->flush();

            return $this->redirectToRoute('watch', array('id' => $id, 'title' => $title));
        }

        return array('video' => $video,
                    'commentaires' => $commentaires,
                    'form' => $form->createView());
    }

    /**
     * @Route("aboutus", name="aboutus")
     * @Template()
     */
     public function aboutusAction()
     {
         return [];
     }

     /**
      * @ROute("login", name="login")
      * @Template()
      */
      public function loginAction()
      {
          return [];
      }
}

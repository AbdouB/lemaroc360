<?php

/**
* @author Abdelilah Benhima <abdelilahbenhima@gmail.com>
*/

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AppBundle\Resources\views;
use AppBundle\Entity\Videos;
use AppBundle\Entity\Tags;
use AppBundle\Entity\Commentaires;
use AppBundle\Validator\Constraints\YouTubeURL;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Knp\Component\Pager\Paginator;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/admin")
 */
class AdminController extends Controller
{
    /**
     * @Route("/", name="adminpanel")
     * @Template()
     */
    public function indexAction()
    {
        return $this->forward('AppBundle:Admin:videos');
    }

    /**
     * @Route("/videos", name="adminvideos")
     * @Template()
     */
     public function videosAction(Request $request)
     {

         $em = $this->getDoctrine()->getManager();
         $dql = "SELECT video FROM AppBundle:Videos video";
         $query = $em->createQuery($dql);
         $paginator = $this->get('knp_paginator');
         $result = $paginator->paginate($query, $request->query->getInt('page', 1), 20);


         return array ('videos' => $result);
     }

     /**
     * @Route("/video/delete/{id}", name="deletevideo")
     */
     public function deletevideoAction($id)
     {

         $em = $this->getDoctrine()->getManager();
         $video = $em->getRepository('AppBundle:Videos')->findOneBy(array('id' => $id));
         $em->remove($video);
         $em->flush();

         return $this->redirectToRoute('adminvideos');
     }

    /**
     * @Route("/video/verify/{id}", name="verifyvideo")
     */
     public function verifyvideoAction($id)
     {

         $em = $this->getDoctrine()->getManager();
         $video = $em->getRepository('AppBundle:Videos')->findOneBy(array('id' => $id));
         $video->setEtat(true);
         $em->persist($video);
         $em->flush();

         return $this->redirectToRoute('adminvideos');
     }

     

     /**
      * @Route("/tags", name="admintags")
      * @Template()
      */
      public function tagsAction(Request $request)
      {
          $em = $this->getDoctrine()->getManager();
          $dql = "SELECT tag FROM AppBundle:Tags tag";
          $query = $em->createQuery($dql);
          $paginator = $this->get('knp_paginator');
          $result = $paginator->paginate($query, $request->query->getInt('page', 1), 5);

          $tag = new Tags();

          $form = $this->createFormBuilder($tag)
            ->add('tag')
            ->add('Ajouter', SubmitType::Class)
            ->getForm();
            
          $form->handleRequest($request);

          if($form->isSubmitted() && $form->isValid())
          {
            $tag = $form->getData();
            $em->persist($tag);
            $em->flush();

            return $this->redirectToRoute('admintags');
          }

          return array('tags' => $result, 'form' => $form->createView());
      }

      /**
       * @Route("/tag/delete/{id}", name="deletetag")
       */
       public function deletetagAction($id)
       {
           $em = $this->getDoctrine()->getManager();
           $tag = $em->getRepository('AppBundle:Tags')->findOneBy(array('id' => $id));
           $em->remove($tag);
           $em->flush();

           return $this->redirectToRoute('admintags'); 
       }



}
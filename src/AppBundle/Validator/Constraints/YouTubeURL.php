<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

class YouTubeURL extends Constraint
{
    public $message = 'Ce n\'est pas un lien valid ';
}
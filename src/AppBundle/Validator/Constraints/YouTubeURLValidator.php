<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class YouTubeURLValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        $rx = '~
            ^(?:https?://)?              # Optional protocol
             (?:www\.)?                  # Optional subdomain
             (?:youtube\.com|youtu\.be)/ # Mandatory domain name
             (?:watch\?v=)?              # optional part
             (?:[a-zA-Z0-9_]{11})        # mandatory video id
             ~x'
        ;

        if (!preg_match($rx, $value))
        {
            $this->context->buildViolation($constraint->message)
                 ->addViolation();
        }
    }
}